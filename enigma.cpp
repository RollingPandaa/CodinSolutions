#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/

int findIndexOf(char c, string rotor)
{
    int result(-1);
    int index(0);
    while(result < 0 && index < rotor.size())
    {
        if(rotor.at(index) == c)
            result = index;
        else
            ++index;
    }
    return result;
};

int main()
{
    string operation;
    getline(cin, operation);
    int pseudo_random_number;
    cin >> pseudo_random_number; cin.ignore();
    vector<string> rotors;
    for (int k = 0; k < 3; k++) {
        string rotor;
        getline(cin, rotor);
        rotors.push_back(rotor);
    }
    string message;
    getline(cin, message);

    // Write an answer using cout. DON'T FORGET THE "<< endl"
    // To debug: cerr << "Debug messages..." << endl;

    cerr << int('A') << " -> " << int('Z') << endl;
    if(operation == "ENCODE")
    {
        for(int i = 0; i < message.size(); ++i)
        {
           int c(int(message.at(i)) + pseudo_random_number + i);
           if(c > 'Z')
                c = c - 'Z' + 'A' - 1;
            message[i] = char(c);

            for(int r = 0; r < rotors.size(); ++r)
            {   
                //cerr << r << " / " << rotors.size() << " - " << int(message.at(i)) << " and " << (message.at(i) - 'A') % 26 << " / " << message.size() << endl;
                message[i] = rotors.at(r).at((message.at(i) - 'A') % 26);
            }
        }
    }
    else
    {
        for(int i = 0; i < message.size(); ++i)
        {
            for(int r = rotors.size() - 1; r >= 0; --r)
                message[i] = char('A' + findIndexOf(message.at(i), rotors.at(r)));

            int c(int(message.at(i)) - pseudo_random_number - i);
            if(c < 'A')
                c = 'Z' - 'A' + c + 1;

            message[i] = char(c);
        }
    }

    cout << message << endl;
}