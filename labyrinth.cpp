#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <algorithm>
#include <cmath>
#include <map>

/*
 * Algorithme A* :
 * - Set a node a cost : here the minimum distance from start
 * - Estimate the distance to the goal
 * - Set the heuristic : cost + estimation
 * - Add the node to the open list
 * While open list is not empty :
 * - Take the first node in open list (the lesser heuristic)
 * - - If the open list is empty, there is no path to the goal
 * - - if the taken node is the goal : rebuild the final path
 * - for each neighboor :
 * - - compute the cost then the heuristic
 * - - add it to the open list
 * - Add the node to the closed list
 */

using namespace std;

enum class TileType {
	Unknown = '?',
	Wall = '#',
	Path = '.',
	Start = 'T',
	Command = 'C'
};

enum class Direction {
	North,
	South,
	East,
	West,
	Any
};

enum class ActionPhase {
	Explore,
	CommandWithoutPath,
	StepBack,
	ReachCommand,
	Return,
	Error
};

string directionOrder(Direction d)
{
	string res;
	switch(d)
	{
		case Direction::North:
			res = "UP";
			break;
		case Direction::South:
			res = "DOWN";
			break;
		case Direction::West:
			res = "LEFT";
			break;
		case Direction::East:
			res = "RIGHT";
			break;
		default:
			res = "ANY";
			break;
	}
	return res;
}

string statusString(ActionPhase ap)
{
	switch (ap) {
		case ActionPhase::Explore:
			return "Explore";
			break;
		case ActionPhase::CommandWithoutPath:
			return "Command without path";
			break;
		case ActionPhase::ReachCommand:
			return "Reach command";
			break;
		case ActionPhase::StepBack:
			return "Step back";
			break;
		case ActionPhase::Return:
			return "Return";
			break;
		default:
			return "Error";
			break;
	}
}

namespace Graph
{
	struct Node {
			int x;
			int y;
			TileType type;
			long cost;
			float heuristic;
			bool deadEnd;

			Node(int x, int y, TileType type = TileType::Unknown, long cost = -1, float heuristic = -1.0):
				x(x), y(y), type(type), cost(cost), heuristic(heuristic), deadEnd(false) {}

			string toString() const { return string("(") + to_string(x) + "," + to_string(y) + "," + char(type) + "," + to_string(cost) + "," + to_string(heuristic) + ")"; }
	};

	bool operator ==(const Node& one, const Node& other) {
		return one.x == other.x && one.y == other.y;
	}

	bool operator <(const Node& one, const Node& other) {
		return (one.x + one.y < other.x + other.y) || (one.x + one.y == other.x + other.y && one.x < other.x ) || (one.x + one.y == other.x + other.y && one.x == other.x && one.y < other.y);
	}

	void outputList(string title, list<Node> nodes)
	{
		cerr << title << " :" << endl;
		for(Node n : nodes)
			cerr << n.toString();
		cerr << endl;
	}

	//	void outputList(string title, set<Node> nodes)
	//	{
	//		cerr << title << " :" << endl;
	//		for(Node n : nodes)
	//			cerr << n.toString();
	//		cerr << endl;
	//	}

	vector<vector<Node> > grid;
	Node startNode(-1, -1);
	Node commandNode(-1, -1);

	bool isValid(const Node& n)
	{
		return n.x >= 0 && n.y >= 0 && n.x < int(grid.size()) && n.y < int(grid[n.x].size());
	}

	void updateCosts(const Node& n, long cost, float heuristic)
	{
		if( isValid(n) && ( grid[n.x][n.y].cost == -1 || cost < grid[n.x][n.y].cost ) )
		{
			grid[n.x][n.y].cost = cost;
			grid[n.x][n.y].heuristic = heuristic;
		}
	}

	void updateCosts(const Node& n)
	{
		updateCosts(n, n.cost, n.heuristic);
	}

	void setDeadEnd(Node& n)
	{
		grid[n.x][n.y].deadEnd = true;
		n.deadEnd = true;
	}

	bool isBorder(const Node& n, Direction d = Direction::Any)
	{
		return ( n.x == 0 && ( d == Direction::North || d == Direction::Any ) )
				|| ( n.y == 0 && ( d == Direction::West || d == Direction::Any ) )
				|| ( n.x == int(grid.size() - 1) && ( d == Direction::South || d == Direction::Any ) )
				|| ( n.y == int(grid[n.x].size() - 1) && ( d == Direction::East || d == Direction::Any ) );
	}

	bool isCrossable(const Node& n)
	{
		return isValid(n) && ( n.type == TileType::Start || n.type == TileType::Command || n.type == TileType::Path );
	}

	Node neighboorOf(const Node& n, Direction d)
	{
		Node res(-1, -1);

		if( isValid(n) && !( d == Direction::Any || isBorder(n, d) ) )
		{
			switch(d)
			{
				case Direction::North:
					res = grid[n.x - 1][n.y];
					break;
				case Direction::South:
					res = grid[n.x + 1][n.y];
					break;
				case Direction::East:
					res = grid[n.x][n.y + 1];
					break;
				case Direction::West:
					res = grid[n.x][n.y - 1];
					break;
				default:
					break;
			}
		}

		return res;
	}

	list<Node> neighboorsOf(const Node& n)
	{
		list<Node> res;

		if( isValid(n))
		{
			if( !isBorder( n, Direction::North ) )
				res.push_back(grid[n.x - 1][n.y]);

			if( !isBorder( n, Direction::West ) )
				res.push_back(grid[n.x][n.y - 1]);

			if( !isBorder( n, Direction::South ) )
				res.push_back(grid[n.x + 1][n.y]);

			if( !isBorder( n, Direction::East ) )
				res.push_back(grid[n.x][n.y + 1]);
		}

		return res;
	}

	list<Node> knownNeighboorsOf(const Node& n)
	{
		list<Node> res;

		for(Node n : neighboorsOf(n))
		{
			if(n.type != TileType::Unknown)
				res.push_back(n);
		}

		return res;
	}

	list<Node> crossableNeighboorsOf(const Node& node)
	{
		list<Node> res;

		for(Node n : knownNeighboorsOf(node))
		{
			if(isCrossable(n))
				res.push_back(n);
		}

		return res;
	}

	Direction getDirection(const Node& from, const Node& to)
	{
		Direction res(Direction::Any);
		if( abs(from.x + from.y - to.x - to.y) == 1 )
		{
			if(from.x < to.x)
				res = Direction::South;
			else if(from.x > to.x)
				res = Direction::North;
			else if(from.y < to.y)
				res = Direction::East;
			else if(from.y > to.y)
				res = Direction::West;
		}
		return res;
	}

	float computeHeuristic(const Node& one, const Node& another)
	{
		//	Compute hypothenuse
		return 1.5 * sqrtf( float( powl( abs( one.x - another.x ), 2 ) + powl( abs( one.y - another.y ), 2 ) ) );
	}

	bool contains(const list<Node>& cont, const Node& n)
	{
		return find(cont.begin(), cont.end(), n) != cont.end();
	}

	bool contains(const list<Node>& cont, int x, int y)
	{
		return contains(cont, Node(x, y));
	}

	list<Node> intersection(const list<Node>& one, const list<Node>& two)
	{
		list<Node> result;
		for(const Node& n : one)
		{
			if( contains(two, n) )
				result.push_back(n);
		}
		return result;
	}

	Node getMinimuHeuristic(list<Node> container)
	{
		Node res(container.back());
		for(Node n : container)
		{
			if(n.heuristic < res.heuristic)
				res = n;
		}
		return res;
	}

	bool isRevealed(const Node& n)
	//	i.e. the type of the node itself and all it's neighboors are known
	{
		if( isValid(n) )
		{
			bool res(true);

			for(Node neib : neighboorsOf(n))
			{
				if(neib.type == TileType::Unknown)
					res = false;
			}

			return res;
		}
		else
			return false;
	}

	void outputGrid()
	{
		cerr << "------------------------ Debug grid begin ----------------------" << endl;

		auto it(grid.cbegin());
		while( it != grid.cend())
		{
			cerr << "| ";
			vector<Node>::const_iterator cell((*it).cbegin());
			while(cell != (*it).cend())
			{
				cerr << ((*cell).deadEnd ? '*' : char((*cell).type)) << " ";
				++cell;
			}
			cerr << " |" << endl;
			++it;
		}
		cerr << "------------------------ Debug grid end ------------------------" << endl;
	}

	list<Node> computePath(Node& from, Node& dest)
	{
		cerr << "Computing path from " << from.toString() << " to " << dest.toString() << endl;
		for(const vector<Node> &row : grid)
		{
			for(Node c : row)
			{
				updateCosts(c, -1, 0.0);
			}
		}
		list<Node> openList;
		list<Node> closedList;
		list<Node> pathlist;

		from.cost = 0;
		from.heuristic = computeHeuristic(from, dest);
		updateCosts(from);
		openList.push_back(from);

		while(!openList.empty())
		{
			Node n(getMinimuHeuristic(openList));
			openList.remove(n);

			cerr << "Open depiled : " << n.toString();

			for(Node v : crossableNeighboorsOf(n) )
			{
				v.cost = n.cost + 1;
				v.heuristic = v.cost + computeHeuristic(v, dest);
				updateCosts(v);

				if( v == dest)
				{
					pathlist.push_back(v);
					pathlist.push_back(n);

					outputList("Destination found", pathlist);

					while(!closedList.empty() && !(pathlist.back() == from))
					{
						list<Node> neighboors( crossableNeighboorsOf(pathlist.back()) );
						outputList("Neighboors", neighboors);
						Node back(neighboors.front());
						for(Node neib : neighboors )
						{
							cerr << "Back neighboor : " << neib.toString() << endl;
							if( neib.cost >= 0 && (neib.cost < back.cost || ( neib.cost == back.cost && neib.heuristic < back.heuristic ) ) && !contains(pathlist, neib) )
								back = neib;
						}

						cerr << "Next back node : " << back.toString() << endl;

						if(isValid(back))
						{
							pathlist.push_back(back);
							closedList.remove(back);
						}
						else
						{
							cerr << "ERROR : No crossable neighboor found !" << endl;
							throw "ERROR : No crossable neighboor found !";
						}
					}
					pathlist.reverse();
					return pathlist;
				}
				else
				{
					if( !( (contains(openList, v) && (*find(openList.begin(), openList.end(), v)).heuristic < v.heuristic) || (contains(closedList, v) ) ) )
					{
						openList.push_back(Node(v.x, v.y, v.type, v.cost, v.heuristic));
					}
				}
			}
			closedList.push_back(n);
		}
		cerr << "Openlist is empty" << endl;
		return list<Node>();
	}
}

int main()
{
	int rows; // number of rows.
	int cols; // number of columns.
	int a; // number of rounds between the time the alarm countdown is activated and the time the alarm goes off.
	cin >> rows >> cols >> a; cin.ignore();

	Graph::grid = vector<vector<Graph::Node> >(rows, vector<Graph::Node>(cols, Graph::Node(-1, -1)));

	ActionPhase status(ActionPhase::Explore);
	list<Direction> priority;
	Direction lastDirection(Direction::Any);
	list<Graph::Node> explored;
	list<Graph::Node> pathToFollow;

	// game loop
	while (1) {
		int kr; // row where Rick is located.
		int kc; // column where Rick is located.
		cin >> kr >> kc; cin.ignore();

		if(priority.empty())
		{
			if(cols > rows)
			{
				priority.push_front(cols - kc >= kc ? Direction::East : Direction::West);
				priority.push_front(rows - kr >= kr ? Direction::South : Direction::North);
				priority.push_front(cols - kc >= kc ? Direction::West : Direction::East);
				priority.push_front(rows - kr >= kr ? Direction::North : Direction::South);
			}
			else
			{
				priority.push_front(rows - kr >= kr ? Direction::South : Direction::North);
				priority.push_front(cols - kc >= kc ? Direction::East : Direction::West);
				priority.push_front(rows - kr >= kr ? Direction::North : Direction::South);
				priority.push_front(cols - kc >= kc ? Direction::West : Direction::East);
			}

			cerr << "Priority : ";
			for(Direction d : priority)
				cerr << directionOrder(d) << " ";
			cerr << endl;
		}

		for (int x = 0; x < rows; x++) {
			string row; // C of the characters in '#.TC?' (i.e. one line of the ASCII maze).
			cin >> row; cin.ignore();

			for(int y = 0; y < int(row.size()); ++y)
			{
				TileType type(TileType(row.at(y)));
				if(type != TileType::Unknown && Graph::grid[x][y].type == TileType::Unknown)	//	Reveal new tile
				{
					Graph::Node n(x, y, type);
					Graph::grid[n.x][n.y] = n;

					if(n.type == TileType::Start)
						Graph::startNode = n;
					else if(n.type == TileType::Command)
						Graph::commandNode = n;
				}
			}
		}

		Graph::outputGrid();

		Graph::Node current(Graph::grid[kr][kc]);
		cerr << "Last direction : " << directionOrder(lastDirection) << endl << "Current position : " << current.toString() << endl;

		bool execute(false);	//	To send the direction order after correctly computed
		while( !execute )
		{
			cerr << "Current state : " << statusString(status) << endl;
			if(pathToFollow.empty())
			{
				switch(status)
				{
					case ActionPhase::Explore:
					case ActionPhase::CommandWithoutPath:
						//	Searching the command post
					{
						if( Graph::isValid(Graph::commandNode) && status == ActionPhase::Explore )
						{
							status = ActionPhase::ReachCommand;
						}
						else
						{
							if( !Graph::isCrossable( Graph::neighboorOf( current, lastDirection ) ) || Graph::neighboorOf( current, lastDirection ).deadEnd || Graph::contains(explored, Graph::neighboorOf( current, lastDirection ) ) )
								lastDirection = Direction::Any;
							//	else continue toward the same direction

							if( lastDirection == Direction::Any )
							{
								auto it(priority.cbegin());
								do
								{
									Graph::Node candidate( Graph::neighboorOf( current, *it ) );
									cerr << "Candidate : " << candidate.toString() << " " << (Graph::isCrossable( candidate ) ? "true" : "false") << " " << (Graph::contains( explored, candidate ) ? "true" : "false") << endl;
									if( Graph::isCrossable( candidate ) && !candidate.deadEnd && !Graph::contains( explored, candidate ) )
										lastDirection = *it;
									else
										++it;
									cerr << directionOrder(lastDirection) << endl;
								}
								while( lastDirection == Direction::Any && it != priority.cend() );

								if(lastDirection == Direction::Any)
									//	It's a dead end
								{
									Graph::setDeadEnd(current);
									status = ActionPhase::StepBack;
								}
								else
									execute = true;
							}
							else
								execute = true;

							if(status == ActionPhase::CommandWithoutPath)
								status = ActionPhase::Explore;
						}
						break;
					}

					case ActionPhase::StepBack:
						//	Step back out of a dead end
					{
						//	Search the last non-explored way
						auto it(explored.crbegin());
						Graph::Node cross(-1, -1);
						while( it != explored.crend() && !Graph::isValid(cross) )
						{
							list<Graph::Node> stepBack(Graph::crossableNeighboorsOf(*it));
							for(Graph::Node n : stepBack)
							{
								if( !Graph::contains(explored, n))
									cross = *it;
							}
							++it;
						}

						if( Graph::isValid(cross) )
						{
							pathToFollow = Graph::computePath(current, cross);

							if(pathToFollow.empty())
							{
								cerr << "ERROR : No path found to way back ! How is it even possible ?";
								throw "ERROR : No path found to way back ! How is it even possible ?";
							}
						}
						else
						{
							cerr << "ERROR : There is no way back !";
							throw "ERROR : There is no way back !";
						}
						break;
					}

					case ActionPhase::ReachCommand:
						//	Compute path to the command post then follow it
					{
						pathToFollow = Graph::computePath(current, Graph::commandNode);

						if(pathToFollow.empty())
							//	No path found to command, continue to explore
						{
							status = ActionPhase::CommandWithoutPath;
						}
						break;
					}

					case ActionPhase::Return:
						//	Compute path to the start point then follow it
					{
						pathToFollow = Graph::computePath(current, Graph::startNode );
						break;
					}

					default:
						cerr << "Action Error" << endl;
						throw "Action error";
						break;
				}
			}
			else
				//	Follow the path
			{
				if(pathToFollow.front() == current)
					pathToFollow.pop_front();

				if( !pathToFollow.empty() )
				{
					Graph::Node nextNode(pathToFollow.front());
					pathToFollow.pop_front();
					lastDirection = Graph::getDirection(current, nextNode);
					execute = true;

					if(status == ActionPhase::StepBack)
						//	Mark all the way as a dead end when stepping back
						Graph::setDeadEnd(current);

					if( pathToFollow.empty())
						//	End of path reached after the next instruction
					{
						switch(status)
						{
							case ActionPhase::StepBack:
							{
								//	Return to exploration when reached explorable way
								status = ActionPhase::Explore;
								break;
							}

							case ActionPhase::ReachCommand:
							{
								status = ActionPhase::Return;
								break;
							}

							case ActionPhase::Return:
							{
								//	End of exercice
								break;
							}

							case ActionPhase::Explore:
							default:
								cerr << "Action Error" << endl;
								throw "Action error";
								break;
						}
					}
				}
			}
		}

		// Write an action using cout. DON'T FORGET THE "<< endl"
		// To debug: cerr << "Debug messages..." << endl;

		if(lastDirection != Direction::Any && !Graph::contains( explored, Graph::neighboorOf(current, lastDirection)) )
			explored.push_back(Graph::neighboorOf(current, lastDirection));

		cerr << "New state : " << statusString(status) << endl;

		cout << directionOrder(lastDirection) << endl; // Rick's next move (UP DOWN LEFT or RIGHT).
	}
}
